#!/usr/bin/python3
'''
Author: Pyro

Purpose: a simple script to administer a Wireguard server after it's
been installed

Description:
The add_client function will add the client to the peers list in the 
server config file and generate a client config file for it.

The del_client function will remove the client from the server config
file, and delete the client config file if one exists

The update_client function will generate a new key pair and replace the 
client config file if one exists and update the server config to reflect
the new key pair

These functions will tied togehter using the menu library (that I wrote)
'''

import menu
import os

wg_conf = "/etc/wireguard/wg0.conf" # server config file
client_folder = "/root/wireguard_clients" # folder to put client configs
ifconfig = os.popen('ifconfig | grep inet').read()
ifconfig = ifconfig.split('\n')[0]
ifconfig = ifconfig.strip()
server_public_ip = ifconfig.split(' ')[1]
ipv4_subnet = '10.66.66.'
ipv6_subnet = 'fd42:42:42::'
usedipv4 = [1]
usedipv6 = [1]
server_publickey = 'did not get overwritten'
server_port = 'did not get overwritten'


def add_client():
	name = input('nickname for the client? $>') # name the client
	privkey = os.popen('wg genkey').read() # generate the private key
	privkey = privkey.split('/n')[0]
	public = os.popen('echo "{}" | wg pubkey'.format(privkey)).read() # generate the public key
	public = public.split('/n')[0]
	v4used = usedipv4[-1].split('.')[-1]
	v4used = v4used.split('/')[0]
	v6used = usedipv6[-1].split('::')[-1]
	v6used = v6used.split("/")[0]
	ipv4 = int(v4used) + 1
	ipv4 = "{}{}/24".format(ipv4_subnet, ipv4)
	ipv6 = int(v6used) + 1
	ipv6 = "{}{}/64".format(ipv6_subnet, ipv6)
	with open(wg_conf, 'a') as f:
		f.write('''

[Peer]
# {}
PublicKey = {}
AllowedIps = {},{}
'''.format(name, public, ipv4, ipv6))
	with open("{}/{}".format(client_folder, name), 'w') as f:
		f.write('''
[Interface]
PrivateKey = {}
Address = {},{}
DNS = 1.1.1.1,8.8.8.8

[Peer]
PublicKey = {}
Endpoint = {}:{}
AllowedIPs = 0.0.0.0/0,::/0
'''.format(privkey, ipv4, ipv6, server_publickey, server_public_ip, server_port))
	print('DONE, find your client config file at {}/{}'.format(client_folder, name))

def del_client():
	print('not built yet')
	
	

def mod_client():
	print('not build yet')
	
def main():
	main = menu.menu('main', 'main')
	o = menu.option
	o('main', 'Add a Client', add_client)
	o('main', 'Delete a Client', del_client)
	o('main', 'Modify a Client', mod_client)

	menu.build(main)
	menu.display(main)


if __name__ == "__main__":
	with open(wg_conf, 'r') as f:
		for line in f:
			if 'PrivateKey' in line:
				server_privatekey = line.split(' = ')[1]
				server_publickey = os.popen('echo "{}" | wg pubkey'.format(server_privatekey)).read()
			if 'ListenPort' in line:
				server_port = line.split(' = ')[1]
			if 'Address' in line:
				addresses = line.split(' = ')
				usedipv4.append(addresses[1].split(',')[0])
				usedipv6.append(addresses[1].split(',')[1])
			if 'AllowedIps' in line:
				addresses = line.split(' = ')
				usedipv4.append(addresses[1].split(',')[0])
				usedipv6.append(addresses[1].split(',')[1])	
	main()
