#!/usr/bin/python3
import os
import importlib
import sys
options = []


class menu:
    lines = "============================"
    count = 1

    def __init__(self,name,level):
        self.name = "{}{}{}".format(self.lines, name, self.lines)
        self.menuname = name
        self.optionsdict = {}
        self.count = 1
        self.level = level


class option:
    def __init__(self, menu, option, fun):
        self.menu = menu
        self.option = option
        self.fun = fun
        options.append(self)


def build(name):
    count = 1
    for option in options:
        if option.menu == name.menuname:
            name.optionsdict[name.count] = option.option
            name.count = name.count + 1


def folder2opt(menu, foldername):
    files = os.listdir(foldername)
    o = option
    if '__init__.py' in files:
        importlib.import_module(foldername)
        pack = sys.modules[foldername]
        for file in files:
            if '__' not in file:
                mod = file[:-3]
                omod = getattr(pack, mod)
                func = getattr(omod, 'main')
                o(menu, mod, func)


def display(name):
    if name.level == 'main':
        loop = True
        while loop:
            print('\n'*100)
            print(name.name)
            for i in range(1 , name.count):
                print("{}.) {}".format(str(i),name.optionsdict[i]))
            print('{}.) exit'.format(str(name.count)))
            print(name.name)
            intake = input('selection? $>')
            if intake == str(name.count):
                print('\n'*100)
                print('Good bye. :-)')
                loop = False
            elif int(intake) < name.count or int(intake) != 0:
                selection = name.optionsdict[int(intake)]
                for option in options:
                    if option.option == selection:
                        option.fun()
                        input("press enter to return")
            else:
                print ("invalid selection")
    elif name.level == 'sub':
        subloop = True
        print('\n'*100)
        while subloop:
            print(name.name)
            for i in range(1 , name.count):
                print(str(i) + '.) ' + name.optionsdict[i])
            print(str(name.count) + '.) ' + 'return to previous menu')
            print(name.name)
            intake = input('selection? $>')
            if intake == str(name.count):
                subloop = False
            else:
                selection = name.optionsdict[int(intake)]
                for option in options:
                    if option.option == selection:
                        option.fun()
                        res = input('clear the screen? y/n $>')
                        if res == 'y':
                            print('\n'*100)
